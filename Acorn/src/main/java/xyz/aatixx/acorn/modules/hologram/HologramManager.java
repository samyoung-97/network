package xyz.aatixx.acorn.modules.hologram;

import net.minecraft.server.v1_15_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_15_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import xyz.aatixx.acorn.Acorn;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class HologramManager { //TODO :: convert this to being cross version compatible

    private Acorn ac;
    private YamlConfiguration h;
    private HashSet<Hologram> holograms;

    public HologramManager(Acorn ac) {
        this.ac = ac;
        h = ac.fs().hologram();
        this.holograms = new HashSet<>();

        load();
    }

    public HashSet<Hologram> holograms() { return holograms; }

    public Hologram hologram(String name) { return holograms.stream().filter(h -> h.name().matches(name)).findFirst().orElse(null); }

    private void load() {

        for(String s : this.ac.fs().hologram().getConfigurationSection("hologram.").getKeys(false)) {
            Location loc = new Location(Bukkit.getWorld(h.getString("hologram." + s + ".location.world")),
                    h.getInt("hologram." + s + ".location.x"),
                    h.getInt("hologram." + s + ".location.y"),
                    h.getInt("hologram." + s + ".location.z"));

            create(s, loc, h.getStringList("hologram." + s + ".lines"));
        }
    }

    //- Create
    public void create(String name, Location loc, List<String> lines) {
        final Location first = loc;
        List<Integer> ent = new ArrayList<>();
        WorldServer ws = ((CraftWorld)loc.getWorld()).getHandle();
        World w = ((World)loc.getWorld()).getMinecraftWorld();

        for(String s : lines) {
            EntityArmorStand as = new EntityArmorStand(w, ((int)loc.getX()) + 0.5, (int)loc.getY(), ((int)loc.getX()) + 0.5);

            as.setLocation(((int)loc.getX()) + 0.5, (int)loc.getY(), ((int)loc.getX()) + 0.5,0,0);
            as.setCustomName(IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + ChatColor.translateAlternateColorCodes('&', s) + "\"}"));
            as.setCustomNameVisible(true);
            as.setNoGravity(false);
            as.setInvisible(true);

            for(Player pl : Bukkit.getOnlinePlayers()) {
                PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(as);
                ((CraftPlayer)pl).getHandle().playerConnection.sendPacket(packet);
            }

            loc.setY(loc.getY() - 0.25);

            ent.add(as.getId());
        }

        Hologram h = new Hologram(this.ac, name, first, lines, ent);
        holograms.add(h);

    }

    public void deleteHologram(String name) {
        Hologram hg = hologram(name);
        if (hg != null) {
            for(int i : hg.entities()) {
                PacketPlayOutEntityDestroy p = new PacketPlayOutEntityDestroy(i);
                for(Player pl : Bukkit.getOnlinePlayers()) {
                    ((CraftPlayer)pl).getHandle().playerConnection.sendPacket(p);
                }
            }
            this.ac.fs().hologram().set("Holograms." + name, null);
            this.ac.fs().save();
            holograms.remove(hg);
        }

    }
}
