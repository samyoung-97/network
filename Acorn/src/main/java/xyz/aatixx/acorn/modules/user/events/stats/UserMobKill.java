package xyz.aatixx.acorn.modules.user.events.stats;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.modules.user.User;

public class UserMobKill implements Listener {

    private Acorn ac;
    public UserMobKill(Acorn ac) { this.ac = ac; }

    @EventHandler
    public void onEntityKill(EntityDeathEvent e) {

        Entity ent = e.getEntity();

        if(e.getEntity().getKiller() != null) {
            Player pl = e.getEntity().getKiller();
            User u = this.ac.userManager().get(pl);

            if(ent instanceof Monster)
                u.setStats("aggressive-mob-kill", u.stats().getInteger("aggressive-mob-kill") + 1);

            if(ent instanceof Mob)
                u.setStats("passive-mob-kill", u.stats().getInteger("passive-mob-kill") + 1);
        }

    }
}
