package xyz.aatixx.acorn.modules.essentials.event.message;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.dev.messages.MessageCentre;

public class PingList implements Listener {

    private Acorn ac;
    public PingList(Acorn ac) { this.ac = ac; }

    @EventHandler
    public void onPingList(ServerListPingEvent e) {
        e.setMotd(MessageCentre.centerMOTD(ac.fs().config().getString("ping-list.line-one")) + "\n" + MessageCentre.centerMOTD(ac.fs().config().getString("ping-list.line-two")));
    }
}
