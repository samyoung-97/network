package xyz.aatixx.acorn.modules.permission;

import org.bson.Document;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import xyz.aatixx.acorn.Acorn;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class Rank {

    private String name;
    private File file;
    private Document data;
    private Acorn acorn;
    private boolean mongo;

    //-
    private Document chat;
    private int priority;
    private List<String> inheritance, permission;

    public Rank(Acorn acorn, String name) { //TODO :: generate ALL ranks based on config list on server start
        this.name = name;

        this.acorn = acorn;
        this.mongo = acorn.fs().config().getString("settings.file-type").matches("MONGO");
        this.file = new File(acorn.fs().rankFolder(), name + ".json");

        if(mongo) { data = acorn.mongo().ranks().find(eq("name", name)).first(); }
        else {
            try {
                if(!file.exists()) file.createNewFile();
            } catch (IOException e) { e.printStackTrace(); }

            if(file.length() != 0) {
                try (FileReader r = new FileReader(file)) {
                    data = Document.parse(new JSONParser().parse(r).toString());
                } catch (IOException | ParseException e) {
                    data = null;
                    e.printStackTrace();
                }
            } else data = null;
        }

        //-
        if(data == null) {
            data = new Document()
                .append("name", name)
                .append("priority", 1)

                .append("chat", new Document()
                    .append("prefix", "&8[&f" + name + "&8]")
                    .append("suffix", "&7:")
                    .append("name-color", "&7")
                    .append("chat-color", "&7")
                )

                .append("inheritance", new ArrayList<>())
                .append("permission", new ArrayList<>())
            ;

            if(mongo) this.acorn.mongo().ranks().insertOne(data);
            else {
                try (FileWriter f = new FileWriter(file)) {
                    f.write(data.toJson());
                    f.flush();
                    f.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        //- Load data
        this.priority = data.getInteger("priority");
        this.chat = (Document) data.get("chat");
        this.permission = (List<String>) data.get("permission");
        this.inheritance = (List<String>) data.get("inheritance");
    }

    //- Get
    public String name() { return name; }
    public Document chat() { return chat; }
    public int priority() { return priority; }
    public List<String> permissions() { return permission; }
    public List<String> inheritance() { return inheritance; }

    //- Set
    public void setPriority(int set) {
        this.priority = set;
        save();
    }
    public void setChat(String item, Object data) {
        chat.put(item, data);
        save();
    }
    public void setPermission(List<String> set) {
        this.permission = set;
        save();
    }
    public void setInheritance(List<String> set) {
        this.inheritance = set;
        save();
    }

    //- Save
    public void save() {
        Document doc = new Document()
            .append("name", name)
            .append("priority", priority)
            .append("chat", chat)
            .append("inheritance", inheritance)
            .append("permission", permission);

        if(mongo) { this.acorn.mongo().ranks().updateOne(eq("name", name), doc); }
        else {
            try (FileWriter f = new FileWriter(file)) {
                f.write(doc.toJson());
                f.flush();
                f.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}