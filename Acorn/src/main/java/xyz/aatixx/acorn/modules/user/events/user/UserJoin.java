package xyz.aatixx.acorn.modules.user.events.user;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.dev.messages.MessageCentre;
import xyz.aatixx.acorn.framework.enums.AMessage;
import xyz.aatixx.acorn.framework.enums.APermission;
import xyz.aatixx.acorn.framework.enums.ASetting;
import xyz.aatixx.acorn.modules.user.User;

public class UserJoin implements Listener {

    private Acorn ac;
    public UserJoin(Acorn ac) { this.ac = ac; }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);

        Player player = e.getPlayer();
        User u = this.ac.userManager().create(player);

        //- Permissions
        this.ac.permissions().createAttachment(player);
        this.ac.permissions().setPermission(player);

        //- Time tracking
        if(ASetting.USER_STAT_TRACKING.b()) u.setPlaytime("login", System.currentTimeMillis());

        //- Essential Messages
        if(player.hasPermission(APermission.JOIN_RANK_BROADCAST.p())) {
            Bukkit.broadcastMessage(MessageCentre.chatCentre(AMessage.RANK_BROADCAST.m()
                    .replace("%rank_prefix%", this.ac.permissions().rank(u.info().getString("rank")).chat().getString("prefix"))
                    .replace("%player_nickname%", u.cosmetic().getString("nickname")))
            );
        }

        for(String s : this.ac.fs().message().getStringList("join.player-join")) {
            player.sendMessage(MessageCentre.chatCentre(s
                    .replace("%player_nickname%", u.cosmetic().getString("nickname"))
            ));
        }
    }
}
