package xyz.aatixx.acorn.modules.user.events.stats;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.modules.user.User;

public class UserMove implements Listener {

    private Acorn ac;
    public UserMove(Acorn ac) { this.ac = ac; }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        Player pl = e.getPlayer();
        User u = this.ac.userManager().get(pl);

        Location from = e.getFrom();
        Location to = e.getTo();

        //- X
        if(from.getX() != to.getX()) {
            double diff = from.getX() > to.getX() ? from.getX() - to.getX() : to.getX() - from.getX();
            if(diff >= 1.00) {
                u.setStats("distance-travel", u.stats().getInteger("distance-travel") + ((int) diff));
            }
        }

        //- Z
        if(from.getZ() != to.getZ()) {
            double diff = from.getZ() > to.getZ() ? from.getZ() - to.getZ() : to.getZ() - from.getZ();
            if(diff >= 1.00) {
                u.setStats("distance-travel", u.stats().getInteger("distance-travel") + ((int) diff));
            }
        }

    }
}
