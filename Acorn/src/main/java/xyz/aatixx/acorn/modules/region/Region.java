package xyz.aatixx.acorn.modules.region;

import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import xyz.aatixx.acorn.Acorn;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Region {

    private Acorn ac;
    private String name;
    private Location one, two;
    private List<String> flags;

    private File file;
    private Document data;

    public Region(Acorn ac, String name) {

        this.ac = ac;
        this.name = name;

        //- File
        file = new File(ac.fs().regionFolder(), name + ".json");
        if(!file.exists()) {
            try { file.createNewFile(); }
            catch (IOException e) { e.printStackTrace(); }
        }

        //- Reading File
        try (FileReader r = new FileReader(file)){
            data = Document.parse(new JSONParser().parse(r).toString());
        } catch (IOException | ParseException e) {
            data = null;
            e.printStackTrace();
        }

        //- Loading Data
        this.flags = (List<String>) data.get("flags");
        this.one = location((Document) data.get("one"));
        this.two = location((Document) data.get("two"));

    }

    public Region(Acorn ac, String name, Location one, Location two) {
        this.ac = ac;
        this.name = name;

        //- File
        file = new File(ac.fs().regionFolder(), name + ".json");
        if(!file.exists()) {
            try { file.createNewFile(); }
            catch (IOException e) { e.printStackTrace(); }
        }

        if(file.length() == 0) {
            data = new Document()
                .append("name", name)
                .append("one", new Document()
                    .append("world", one.getWorld().getName())
                    .append("x", (int) one.getX())
                    .append("y", (int) one.getY())
                    .append("z", (int) one.getX())
                )
                .append("two", new Document()
                    .append("world", two.getWorld().getName())
                    .append("x", (int) two.getX())
                    .append("y", (int) two.getY())
                    .append("z", (int) two.getX())
                )
                .append("flags", new ArrayList<>())
            ;

            try (FileWriter f = new FileWriter(file)) {
                f.write(data.toJson());
                f.flush();
                f.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //- Reading File
        try (FileReader r = new FileReader(file)){
            data = Document.parse(new JSONParser().parse(r).toString());
        } catch (IOException | ParseException e) {
            data = null;
            e.printStackTrace();
        }

        //- Loading Data
        this.flags = (List<String>) data.get("flags");
        this.one = location((Document) data.get("one"));
        this.two = location((Document) data.get("two"));
    }


    private Location location(Document d) {
        return new Location(Bukkit.getWorld(d.getString("world")), d.getInteger("x"), d.getInteger("y"), d.getInteger("z"));
    }

    //- Get
    public String name() { return name; }
    public List<String> flags() { return flags; }
    public Location one() { return one; }
    public Location two() { return two; }

    //- Set
    public void setOne(Location set) {
        this.one = set;
        data.put("one", new Document()
            .append("world", set.getWorld().getName())
            .append("x", (int) set.getX())
            .append("y", (int) set.getY())
            .append("z", (int) set.getX())
        );
        save();
    }
    public void setTwo(Location set) {
        this.two = set;
        data.put("two", new Document()
                .append("world", set.getWorld().getName())
                .append("x", (int) set.getX())
                .append("y", (int) set.getY())
                .append("z", (int) set.getX())
        );
        save();
    }
    public void setFlags(List<String> set) {
        this.flags = set;
        save();
    }

    //- Save
    public void save() {
        Document doc = new Document()
            .append("name", name)
            .append("one", new Document()
                .append("world", one.getWorld().getName())
                .append("x", (int) one.getX())
                .append("y", (int) one.getY())
                .append("z", (int) one.getX())
            )
            .append("two", new Document()
                .append("world", two.getWorld().getName())
                .append("x", (int) two.getX())
                .append("y", (int) two.getY())
                .append("z", (int) two.getX())
            )
            .append("flags", flags)
        ;

        try (FileWriter f = new FileWriter(file)) {
            f.write(doc.toJson());
            f.flush();
            f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
