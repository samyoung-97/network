package xyz.aatixx.acorn.modules.hologram;

import org.bukkit.Location;
import xyz.aatixx.acorn.Acorn;

import java.util.List;

public class Hologram {

    private Acorn ac;
    private String name;
    private Location location;
    private List<String> lines;
    private List<Integer> entities;

    public Hologram(Acorn ac, String name, Location location, List<String> lines, List<Integer> entities) {

        //- Setting
        this.ac = ac;
        this.name = name;
        this.location = location;
        this.lines = lines;
        this.entities = entities;

        //- Saving in file
        ac.fs().hologram().set("hologram." + name + ".location.world", location.getWorld().getName());
        ac.fs().hologram().set("hologram." + name + ".location.x", (int) location.getX());
        ac.fs().hologram().set("hologram." + name + ".location.y", (int) location.getY());
        ac.fs().hologram().set("hologram." + name + ".location.z", (int) location.getZ());
        ac.fs().hologram().set("hologram." + name + ".lines", lines);
        ac.fs().hologram().set("hologram." + name + ".entities", entities);
        ac.fs().save();
    }

    //- Get
    public String name() { return name; }
    public Location location() { return location; }
    public List<String> lines() { return lines; }
    public List<Integer> entities() { return entities; }

    //- Set
    public void setLocation(Location set) {
        this.location = set;
        save();
    }
    public void setLines(List<String> set) {
        this.lines = set;
        save();
    }
    public void setEntities(List<Integer> set) {
        this.entities = set;
        save();
    }

    //- Save
    public void save() {
        ac.fs().hologram().set("hologram." + name + ".location.world", location.getWorld().getName());
        ac.fs().hologram().set("hologram." + name + ".location.x", (int) location.getX());
        ac.fs().hologram().set("hologram." + name + ".location.y", (int) location.getY());
        ac.fs().hologram().set("hologram." + name + ".location.z", (int) location.getZ());
        ac.fs().hologram().set("hologram." + name + ".lines", lines);
        ac.fs().hologram().set("hologram." + name + ".entities", entities);
        ac.fs().save();
    }
}