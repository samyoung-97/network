package xyz.aatixx.acorn.modules.region.events.user;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.modules.region.Region;
import xyz.aatixx.acorn.modules.region.events.custom.RegionEnterEvent;
import xyz.aatixx.acorn.modules.region.events.custom.RegionLeaveEvent;

import java.util.Set;

public class PlayerMove implements Listener {

    private Acorn ac;
    public PlayerMove(Acorn ac) { this.ac = ac; }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        Location to = e.getTo();
        Location from = e.getFrom();

        if(to.getX() != from.getX() || to.getY() != from.getY() || to.getZ() != from.getZ()) {

            Set<Region> toSet = this.ac.region().applicable(to);
            Set<Region> fromSet = this.ac.region().applicable(from);

            //- Leaving a region
            for(Region r : fromSet) {
                if(!toSet.contains(r)) this.ac.getServer().getPluginManager().callEvent(new RegionLeaveEvent(player, r, to, from));
            }

            //- Joining a region
            for(Region r : toSet) {
                if(!fromSet.contains(r)) this.ac.getServer().getPluginManager().callEvent(new RegionEnterEvent(player, r, to, from));
            }

        }
    }
}
