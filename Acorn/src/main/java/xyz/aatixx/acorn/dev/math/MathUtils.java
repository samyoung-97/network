package xyz.aatixx.acorn.dev.math;

public class MathUtils {
    public static boolean between(int i, int min, int max) {
        return i >= min && i <= max;
    }

    public static boolean between(double i, double min, double max) {
        return i >= min && i <= max;
    }
}
