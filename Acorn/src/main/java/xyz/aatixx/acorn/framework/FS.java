package xyz.aatixx.acorn.framework;

import org.bukkit.configuration.file.YamlConfiguration;
import xyz.aatixx.acorn.Acorn;

import java.io.File;
import java.io.IOException;

public class FS {

    //- Folder
    private File dataFolder, userFolder, rankFolder, regionFolder;

    //- Files
    private File configF, messageF, permissionF, menuF, hologramF;
    private YamlConfiguration config, message, permission, menu, hologram;

    public FS(Acorn ac) {

        //- Folder
        dataFolder = ac.getDataFolder();
        if(!dataFolder.exists()) dataFolder.mkdirs();
        userFolder = new File(dataFolder + File.separator + "User");
        if(!userFolder.exists()) userFolder.mkdirs();
        rankFolder = new File(dataFolder + File.separator + "Rank");
        if(!rankFolder.exists()) rankFolder.mkdirs();
        regionFolder = new File(dataFolder + File.separator + "Region");
        if(!regionFolder.exists()) regionFolder.mkdirs();

        //- Files
        configF = new File(dataFolder, "Config.yml");
        messageF = new File(dataFolder, "Message.yml");
        permissionF = new File(dataFolder, "Permission.yml");
        menuF = new File(dataFolder, "Menu.yml");
        hologramF = new File(dataFolder, "Hologram.yml");

        if(!configF.exists()) ac.saveResource("Config.yml", false);
        if(!messageF.exists()) ac.saveResource("Message.yml", false);
        if(!permissionF.exists()) ac.saveResource("Permission.yml", false);
        if(!menuF.exists()) ac.saveResource("Menu.yml", false);
        if(!hologramF.exists()) ac.saveResource("Hologram.yml", false);

        config = YamlConfiguration.loadConfiguration(configF);
        message = YamlConfiguration.loadConfiguration(messageF);
        permission = YamlConfiguration.loadConfiguration(permissionF);
        menu = YamlConfiguration.loadConfiguration(menuF);
        hologram = YamlConfiguration.loadConfiguration(hologramF);
    }

    //- Get
    public File userFolder() { return userFolder; }
    public File rankFolder() { return rankFolder; }
    public File regionFolder() { return regionFolder; }

    public YamlConfiguration config() { return config; }
    public YamlConfiguration message() { return message; }
    public YamlConfiguration permission() { return permission; }
    public YamlConfiguration menu() { return menu; }
    public YamlConfiguration hologram() { return hologram; }

    //- Save
    public void save() {
        try {
            config.save(configF);
            message.save(messageF);
            permission.save(permissionF);
            menu.save(menuF);
            hologram.save(hologramF);
        } catch (IOException e) { e.printStackTrace(); }
    }

    //- Reload
    public void reload() {
        config = YamlConfiguration.loadConfiguration(configF);
        message = YamlConfiguration.loadConfiguration(messageF);
        permission = YamlConfiguration.loadConfiguration(permissionF);
        menu = YamlConfiguration.loadConfiguration(menuF);
        hologram = YamlConfiguration.loadConfiguration(hologramF);
    }
}
