package xyz.aatixx.acorn.modules.user;

import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.dev.builder.InventoryBuilder;
import xyz.aatixx.acorn.dev.builder.ItemBuilder;
import xyz.aatixx.acorn.dev.messages.MessageUtil;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class User {

    private Player player;
    private Acorn acorn;
    private Document data;
    private boolean mongo;
    private File file;

    //- Accessible
    private Document info, cosmetic, economy, stats, playtime;
    private List<String> permissions;

    public User(Acorn acorn, Player player) {
        this.mongo = acorn.fs().config().getString("settings.file-type").matches("MONGO");

        this.acorn = acorn;
        this.player = player;
        this.file = new File(acorn.fs().userFolder(), player.getUniqueId().toString() + ".json");
        if(mongo) {
            this.data = acorn.mongo().users().find(eq("uuid", player.getUniqueId().toString())).first();
        } else {
            if(!file.exists()) {
                try { file.createNewFile(); }
                catch (IOException e) { e.printStackTrace(); }
            }

            if(file.length() != 0) {
                try (FileReader r = new FileReader(file)){
                    data = Document.parse(new JSONParser().parse(r).toString());
                } catch (IOException | ParseException e) {
                    data = null;
                    e.printStackTrace();
                }
            } else data = null;
        }


        if(data == null) {
            //- Generating player connection hash
            String hash = player.getAddress().getHostName();
            try {
                MessageDigest md = MessageDigest.getInstance("SHA-512");
                md.update(hash.getBytes());
                byte[] by = md.digest();
                StringBuilder b = new StringBuilder();
                for (byte value : by) { b.append(Integer.toString((value & 0xff) + 0x100, 16).substring(1)); }
                hash = b.toString();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            //-
            data = new Document()
                .append("uuid", player.getUniqueId().toString())
                .append("information", new Document()
                    .append("rank", acorn.fs().config().getString("settings.default-rank"))
                    .append("name", player.getName())
                    .append("connection", hash)
                )
                .append("cosmetic", new Document()
                    .append("nickname", player.getName())
                    .append("name-color", "&7")
                    .append("chat-color", "&7")
                    .append("title", "null")
                )
                .append("economy", new Document()
                    .append("player-balance", acorn.fs().config().getDouble("settings.starting-player-balance"))
                    .append("bank-balance", acorn.fs().config().getDouble("settings.starting-bank-balance"))
                    .append("bank-interest-given", 0) //- Time last given
                    .append("bank-interest-total", 0) //- Total interest
                    .append("bank-interest-last-value", 0) //- The value of the LAST interest amount earned
                )
                .append("statistics", new Document()
                    .append("block-break", 0)
                    .append("block-place", 0)
                    .append("player-kill", 0)
                    .append("player-death", 0)
                    .append("passive-mob-kill", 0)
                    .append("aggressive-mob-kill", 0)
                    .append("distance-travel", 0)
                )
                .append("playtime", new Document()
                    .append("login", 0)
                    .append("logout", 0)
                    .append("total", 0)
                )
            .append("permissions", new ArrayList<>())
            ;


            if(mongo) {
                this.acorn.mongo().users().insertOne(data);
                Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&d" + player.getName() + " inserted into " +
                        "database"));
            }
            else {
                try (FileWriter f = new FileWriter(file)) {
                    f.write(data.toJson());
                    f.flush();
                    f.close();
                    Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&d" + player.getName() + " saved to JSON " +
                            "file"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        //- Load data
        info = (Document) data.get("information");
        cosmetic = (Document) data.get("cosmetic");
        economy = (Document) data.get("economy");
        stats = (Document) data.get("statistics");
        playtime = (Document) data.get("playtime");
        permissions = (List<String>) data.get("permissions");
    }

    //- Get
    public Player player() { return player; }
    public Document info() { return info; }
    public Document cosmetic() { return cosmetic; }
    public Document economy() { return economy; }
    public Document stats() { return stats; }
    public Document playtime() { return playtime; }
    public List<String> permissions() { return permissions; }

    //- Set
    public void setInfo(String item, Object data) {
        info.put(item, data);
        save();
    }
    public void setCosmetic(String item, Object data) {
        cosmetic.put(item, data);
        save();
    }
    public void setEconomy(String item, Object data) {
        economy.put(item, data);
        save();
    }
    public void setStats(String item, Object data) {
        stats.put(item, data);
        save();
    }
    public void setPlaytime(String item, Object data) {
        playtime.put(item, data);
        save();
    }
    public void setPermissions(List<String> set) {
        this.permissions = set;
        save();
    }

    //- Save
    public void save() {
        Document doc = new Document().append("uuid", player.getUniqueId().toString())
            .append("information", info)
            .append("cosmetic", cosmetic)
            .append("economy", economy)
            .append("statistics", stats)
            .append("playtime", playtime)
            .append("permissions", permissions);
        if(this.mongo) this.acorn.mongo().users().updateOne(eq("uuid", player.getUniqueId().toString()), doc);
        else {
            try (FileWriter f = new FileWriter(file)) {
                f.write(doc.toJson());
                f.flush();
                f.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Inventory profile() {
        YamlConfiguration m = this.acorn.fs().menu();

        InventoryBuilder b = new InventoryBuilder();

        b.setSize(m.getInt("player-profile.size"));
        b.setName(m.getString("player-profile.title").replace("%player%", player.getName()));

        b.setFillItem(new ItemBuilder(Material.matchMaterial(m.getString("player-profile.fill-item.material")))
                .setName(m.getString("player-profile.fill-item.name"))
                .hideAttribute()
                .build());

        //- Items
        for(String s : m.getConfigurationSection("player-profile.items.").getKeys(false)) {
            ItemBuilder i = new ItemBuilder(Material.matchMaterial(m.getString("player-profile.items." + s + ".material")));

            i.setName(m.getString("player-profile.items." + s + ".name").replace("%player%", player.getName()));

            if(m.getString("player-profile.items." + s + ".material").matches("PLAYER_HEAD")) {
                i.setSkullOwner(player.getName());
            }

            for(String str : m.getStringList("player-profile.items." + s + ".lore")) {
                i.addLoreLine(str
                        //-
                    .replace("%rank%", info.getString("rank"))
                    .replace("%nickname%", cosmetic.getString("nickname"))
                        //- Economy
                    .replace("%player_balance%", NumberFormat.getCurrencyInstance().format(economy.getDouble("player-balance")))
                    .replace("%bank_balance%", NumberFormat.getCurrencyInstance().format(economy.getDouble("bank-balance")))
                        //- Stats
                    .replace("%block_mined%", NumberFormat.getInstance().format(stats.getInteger("block-break")))
                    .replace("%block_place%", NumberFormat.getInstance().format(stats.getInteger("block-place")) )
                    .replace("%player_kill%", NumberFormat.getInstance().format(stats.getInteger("player-kill")))
                    .replace("%player_death%", NumberFormat.getInstance().format(stats.getInteger("player-death")))
                    .replace("%passive_mob_kill%", NumberFormat.getInstance().format(stats.getInteger("passive-mob-kill")))
                    .replace("%aggressive_mob_kill%", NumberFormat.getInstance().format(stats.getInteger("aggressive-mob-kill")))
                    .replace("%distance_travel%", NumberFormat.getInstance().format(stats.getInteger("distance-travel")))
                        //- Playtime
                    .replace("%playtime%", MessageUtil.timeRemainingLong(playtime.getInteger("total")))
                );
            }

            if(m.getBoolean("player-profile.items." + s + ".glow")) {
                i.addEnchantment(Enchantment.LUCK, 1);
                i.hideEnchants();
            }

            i.hideAttribute();

            b.setItem(m.getInt("player-profile.items." + s + ".slot"), i.build());
        }

        return b.build();
    }

}