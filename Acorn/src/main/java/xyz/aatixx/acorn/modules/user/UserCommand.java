package xyz.aatixx.acorn.modules.user;

import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.modules.user.command.UserProfileCmd;

public class UserCommand {
    public UserCommand(Acorn ac) {

        ac.getServer().getPluginCommand("Profile").setExecutor(new UserProfileCmd(ac));

    }
}
