package xyz.aatixx.acorn;

import org.bukkit.plugin.java.JavaPlugin;
import xyz.aatixx.acorn.framework.FS;
import xyz.aatixx.acorn.framework.Mongo;
import xyz.aatixx.acorn.modules.essentials.EssentialEvents;
import xyz.aatixx.acorn.modules.hologram.HologramManager;
import xyz.aatixx.acorn.modules.permission.Permissions;
import xyz.aatixx.acorn.modules.region.RegionEvents;
import xyz.aatixx.acorn.modules.region.RegionManager;
import xyz.aatixx.acorn.modules.user.UserCommand;
import xyz.aatixx.acorn.modules.user.UserEvents;
import xyz.aatixx.acorn.modules.user.UserManager;

public final class Acorn extends JavaPlugin {

    //- API Access
    private static Acorn api;
    public static Acorn api() { return api; }

    @Override
    public void onEnable() {
        //- API Access
        api = this;

        //- Files
        fs = new FS(this);

        //- Module :: MongoDB
        if(fs.config().getString("settings.file-type").matches("MONGO")) mongo = new Mongo(this);

        //- Module :: Permissions
        permissions = new Permissions(this);

        //- Module :: User
        userManager = new UserManager(this);
        new UserEvents(this);
        new UserCommand(this);

        //- Module :: Region
        region = new RegionManager(this);
        new RegionEvents(this);

        //- Module :: Hologram
        hologram = new HologramManager(this);

        //- Module :: Essentials
        new EssentialEvents(this);
    }

    @Override
    public void onDisable() {
        //TODO :: de-spawn all holograms here
    }

    //- Files
    private FS fs;
    public FS fs() { return fs; }

    //- Module :: MongoDB
    private Mongo mongo;
    public Mongo mongo() { return mongo; }

    //- Module :: Permissions
    private Permissions permissions;
    public Permissions permissions() { return permissions; }

    //- Module :: User
    public UserManager userManager() { return userManager; }
    private UserManager userManager;

    //- Module :: Region
    private RegionManager region;
    public RegionManager region() { return region; }

    //- Module :: Holograms
    private HologramManager hologram;
    public HologramManager hologram() { return hologram; }

}
