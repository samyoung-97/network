package xyz.aatixx.acorn.framework.enums;

import org.bukkit.configuration.file.YamlConfiguration;
import xyz.aatixx.acorn.Acorn;

public enum ASetting {

    USER_STAT_TRACKING("modules.stat-tracking"),
    CHAT_FILTER("modules.chat-filter"),
    CHAT_FORMAT("modules.chat-format"),
    ;

    private String index;
    private YamlConfiguration c;

    ASetting(String index) {
        this.index = index;
        c = Acorn.api().fs().config();
    }

    public boolean b() {
        return c.getBoolean(index);
    }

}