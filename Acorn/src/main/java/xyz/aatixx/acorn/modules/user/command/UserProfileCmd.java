package xyz.aatixx.acorn.modules.user.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import xyz.aatixx.acorn.Acorn;

public class UserProfileCmd implements CommandExecutor {
    private Acorn ac;
    public UserProfileCmd(Acorn ac) { this.ac = ac; }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender instanceof Player) {
            Player pl = (Player) sender;
            pl.openInventory(this.ac.userManager().get(pl).profile());
        }

        return false;
    }
}
