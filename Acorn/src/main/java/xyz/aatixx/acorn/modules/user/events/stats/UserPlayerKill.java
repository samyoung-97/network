package xyz.aatixx.acorn.modules.user.events.stats;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.modules.user.User;

public class UserPlayerKill implements Listener {

    private Acorn ac;
    public UserPlayerKill(Acorn ac) { this.ac = ac; }

    @EventHandler
    public void onKill(PlayerDeathEvent e) {

        //- Death
        Player dead = e.getEntity();
        User du = this.ac.userManager().get(dead);
        du.setStats("player-death", du.stats().getInteger("player-death") + 1);

        //- Kill
        if(e.getEntity().getKiller() != null) {
           Player killer = e.getEntity().getKiller();
           User ku = this.ac.userManager().get(killer);
           ku.setStats("player-kill", ku.stats().getInteger("player-kill") + 1);
        }

    }
}
