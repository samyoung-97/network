package xyz.aatixx.acorn.modules.region;

import org.bukkit.Location;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.dev.math.MathUtils;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class RegionManager {

    private Acorn ac;
    private HashSet<Region> regions;

    public RegionManager(Acorn ac) {
        this.ac = ac;
        this.regions = new HashSet<>();

        for(File f : ac.fs().regionFolder().listFiles()) {
            regions.add(new Region(ac, f.getName().replace(".json", "")));
        }
    }

    public HashSet<Region> regions() { return regions; }

    public Region create(String name, Location one, Location two) {
        Region r = new Region(this.ac, name, one, two);
        regions.add(r);
        return r;
    }

    public Region region(String name) {
        return regions.stream().filter(r -> r.name().matches(name)).findFirst().orElse(null);
    }

    public Set<Region> applicable(Location loc) {
        Set<Region> set = new HashSet<>();

        for(Region r : regions) {
            if( MathUtils.between(loc.getX(), Math.min(r.one().getX(), r.two().getX()), Math.max(r.one().getX(), r.two().getX()))
                    && MathUtils.between(loc.getY(), Math.min(r.one().getY(), r.two().getY()), Math.max(r.one().getY(), r.two().getY()))
                    && MathUtils.between(loc.getZ(), Math.min(r.one().getZ(), r.two().getZ()), Math.max(r.one().getZ(), r.two().getZ())) ) {
                set.add(r);
            }
        }

        return set;
    }

    //TODO :: delete method
}
