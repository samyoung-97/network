package xyz.aatixx.acorn.framework;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import xyz.aatixx.acorn.Acorn;

public class Mongo {
    private MongoClient client = null;
    private MongoDatabase database = null;
    private MongoCollection<Document> users, ranks;

    private MongoClientURI uri;

    public Mongo(Acorn ac) {

        try {
            uri = new MongoClientURI(ac.fs().config().getString("mongodb.connection-uri"));
            client = new MongoClient(uri);
        } catch (Exception e) { e.printStackTrace(); }

        //- Database
        if(client != null) {
            database = client.getDatabase(ac.fs().config().getString("mongodb.database-name"));
        }

        if(database != null) {
            users = database.getCollection(ac.fs().config().getString("mongodb.collection.user"));
            ranks = database.getCollection(ac.fs().config().getString("mongodb.collection.rank"));
        }
    }

    public MongoCollection<Document> users() { return users; }
    public MongoCollection<Document> ranks() { return ranks; }
}
