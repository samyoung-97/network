package xyz.aatixx.acorn.modules.user.events.stats;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.modules.user.User;

public class UserBlockPlace implements Listener {
    private Acorn ac;
    public UserBlockPlace(Acorn ac) { this.ac = ac; }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        Player player = e.getPlayer();
        User u = this.ac.userManager().get(player);

        u.setStats("block-place", u.stats().getInteger("block-place") + 1);
    }
}
