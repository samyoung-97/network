package xyz.aatixx.acorn.modules.region;

import org.bukkit.plugin.PluginManager;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.modules.region.events.user.PlayerMove;

public class RegionEvents {

    public RegionEvents(Acorn ac) {
        PluginManager pm = ac.getServer().getPluginManager();

        pm.registerEvents(new PlayerMove(ac), ac);

    }
}
