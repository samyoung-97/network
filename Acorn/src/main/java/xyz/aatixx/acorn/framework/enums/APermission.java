package xyz.aatixx.acorn.framework.enums;

import org.bukkit.configuration.file.YamlConfiguration;
import xyz.aatixx.acorn.Acorn;

public enum APermission {

    PLAYER_CHAT_NAME_COLOR("chat.player-name-color"),
    PLAYER_CHAT_MESSAGE_COLOR("chat.player-chat-message-color"),

    JOIN_RANK_BROADCAST("join.rank-broadcast"),
    ;

    private String index;
    private YamlConfiguration p;

    APermission(String index) {
        this.index = index;
        p = Acorn.api().fs().permission();
    }

    public String p() { return p.getString(this.index); }
}
