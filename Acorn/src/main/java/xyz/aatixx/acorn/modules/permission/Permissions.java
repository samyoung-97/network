package xyz.aatixx.acorn.modules.permission;

import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.modules.user.User;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;

public class Permissions {

    private Acorn acorn;
    private boolean mongo;
    private HashMap<UUID, PermissionAttachment> attachments;
    private HashSet<Rank> ranks;

    public Permissions(Acorn acorn) {
        this.acorn = acorn;
        this.mongo = acorn.fs().config().getString("settings.file-type").matches("MONGO");

        this.attachments = new HashMap<>();
        this.ranks = new HashSet<>();

        loadRanks();
    }

    //- Ranks
    public HashSet<Rank> ranks() { return ranks; }
    public Rank createRank(String name) {
        Rank r = new Rank(this.acorn, name);
        ranks.add(r);
        return r;
    }
    public Rank rank(String name) { return ranks.stream().filter(r -> r.name().matches(name)).findFirst().orElse(defaultRank()); }

    private Rank defaultRank() {
        Rank r = new Rank(this.acorn, this.acorn.fs().config().getString("settings.default-rank"));
        ranks.add(r);
        return r;
    }

    public void setPlayerRank(Player player, String rank) {
        User u = this.acorn.userManager().get(player);
        u.setInfo("rank", rank);

        unsetPermissions(player);
        setPermission(player);
    }

    public void deleteRank(String rank) {
        Rank r = rank(rank);

        for(User u : this.acorn.userManager().users()) {
            if(u.info().getString("rank").matches(rank)) {
                u.setInfo("rank", this.acorn.fs().config().getString("settings.default-rank"));
                unsetPermissions(u.player());
                setPermission(u.player());
            }
        }

        ranks.remove(r);

        //- Deleting rank from ALL locations
        if(mongo) {
            this.acorn.mongo().ranks().deleteOne(eq("name", rank));
        } else {
            File f = new File(this.acorn.fs().rankFolder(), rank + ".json");
            if(f.exists()) f.delete(); //TODO :: test this...
        }
    }

    public void loadRanks() {
        if(mongo) {
            for(Document d : this.acorn.mongo().ranks().find()) {
                createRank(d.getString("name"));
            }
        } else {
            for(File f : this.acorn.fs().rankFolder().listFiles()) {
                createRank(f.getName().replace(".json", ""));
            }
        }
    }

    public void addInheritance(String rank, String inheritance) {
        Rank r = rank(rank);
        Rank i = rank(inheritance);

        List<String> inherit = r.inheritance();
        inherit.add(inheritance);
        r.setInheritance(inherit);

        for(User u : this.acorn.userManager().users()) {
            if(u.info().getString("rank").matches(rank)) {
                PermissionAttachment pa = attachments.get(u.player().getUniqueId());
                for(String s : i.permissions()) {
                    pa.setPermission(s, true);
                }
                attachments.put(u.player().getUniqueId(), pa);
            }
        }
    }

    public void removeInheritance(String rank, String inheritance) {
        Rank r = rank(rank);
        Rank i = rank(inheritance);

        List<String> inherit = r.inheritance();
        inherit.remove(inheritance);
        r.setInheritance(inherit);

        for(User u : this.acorn.userManager().users()) {
            if(u.info().getString("rank").matches(rank)) {
                PermissionAttachment pa = attachments.get(u.player().getUniqueId());
                for(String s : i.permissions()) {
                    pa.unsetPermission(s);
                }
                attachments.put(u.player().getUniqueId(), pa);
            }
        }
    }

    public void addRankPermission(String rank, String permission) {
        Rank r = rank(rank);
        List<String> perms = r.permissions();
        perms.add(permission);
        r.setPermission(perms);

        for(User u : this.acorn.userManager().users()) {
            if(u.info().getString("rank").matches(rank)) {
                PermissionAttachment pa = attachments.get(u.player().getUniqueId());
                pa.setPermission(permission, true);
                attachments.put(u.player().getUniqueId(), pa);
            }
        }
    }

    public void removeRankPermission(String rank, String permission) {
        Rank r = rank(rank);
        List<String> perms = r.permissions();
        perms.remove(permission);
        r.setPermission(perms);

        for(User u : this.acorn.userManager().users()) {
            if(u.info().getString("rank").matches(rank)) {
                PermissionAttachment pa = attachments.get(u.player().getUniqueId());
                pa.unsetPermission(permission);
                attachments.put(u.player().getUniqueId(), pa);
            }
        }
    }

    public void addPlayerPermission(Player player, String permission) {
        User u = this.acorn.userManager().get(player);
        List<String> perms = u.permissions();
        perms.add(permission);
        u.setPermissions(perms);

        PermissionAttachment pa = attachments.get(player.getUniqueId());
        pa.setPermission(permission, true);
        attachments.put(player.getUniqueId(), pa);
    }

    public void removePlayerPermission(Player player, String permission) {
        User u = this.acorn.userManager().get(player);
        List<String> perms = u.permissions();
        perms.remove(permission);
        u.setPermissions(perms);

        PermissionAttachment pa = attachments.get(player.getUniqueId());
        pa.unsetPermission(permission);
        attachments.put(player.getUniqueId(), pa);
    }

    public void createAttachment(Player player) {
        PermissionAttachment pa = player.addAttachment(this.acorn);
        attachments.put(player.getUniqueId(), pa);
    }

    public void setPermission(Player player) {

        File file = new File(this.acorn.fs().userFolder(), player.getUniqueId().toString() + ".json");
        Document data, information;
        if(mongo) {
            data = acorn.mongo().users().find(eq("uuid", player.getUniqueId().toString())).first();
        } else {
            if(!file.exists()) {
                try { file.createNewFile(); }
                catch (IOException e) { e.printStackTrace(); }
            }

            try (FileReader r = new FileReader(file)){
                data = Document.parse(new JSONParser().parse(r).toString());
            } catch (IOException | ParseException e) {
                data = null;
                e.printStackTrace();
            }
        }

        //-
        if(data != null) {
            information = (Document) data.get("information");
            Rank r = rank(information.getString("rank"));
            PermissionAttachment pa = attachments.get(player.getUniqueId());

            List<String> permissions = (List<String>) data.get("permissions");
            permissions.addAll(r.permissions());
            for(String s : r.inheritance()) {
                Rank in = rank(s);
                permissions.addAll(in.permissions());
            }

            for(String s : permissions) {
                pa.setPermission(s, true);
            }

            attachments.put(player.getUniqueId(), pa);
        }

    }

    public void unsetPermissions(Player player) {
        PermissionAttachment pa = attachments.get(player.getUniqueId());
        pa.getPermissions().keySet().forEach(pa::unsetPermission);
        attachments.remove(player.getUniqueId());
    }
}
