package xyz.aatixx.acorn.modules.user.events.user;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.framework.enums.APermission;
import xyz.aatixx.acorn.framework.enums.ASetting;
import xyz.aatixx.acorn.modules.permission.Rank;
import xyz.aatixx.acorn.modules.user.User;

import java.util.List;

public class UserChat implements Listener {

    private Acorn ac;
    public UserChat(Acorn ac) { this.ac = ac; }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {

        e.setCancelled(true);

        Player player = e.getPlayer();
        String message = ChatColor.translateAlternateColorCodes('&', e.getMessage());
        User u = this.ac.userManager().get(player);
        Rank r = this.ac.permissions().rank(u.info().getString("rank"));

        //-
        String prefix = r.chat().getString("prefix") + " ",
               nickname = u.cosmetic().getString("nickname"),
               suffix = r.chat().getString("suffix") + " ",
               nameColor = player.hasPermission(APermission.PLAYER_CHAT_NAME_COLOR.p()) ? u.cosmetic().getString("name-color") : r.chat().getString("name-color"),
               chatColor = player.hasPermission(APermission.PLAYER_CHAT_MESSAGE_COLOR.p()) ? u.cosmetic().getString("chat-color") : r.chat().getString("chat-color"),
               title = u.cosmetic().getString("title").matches("null") ? "" : " " + u.cosmetic().getString("title") ;
        String msg = ChatColor.stripColor(message);

        //- Chat Filter
        if(ASetting.CHAT_FILTER.b()) {
            List<String> words = this.ac.fs().config().getStringList("chat-filter");
            for(String s : msg.split(" ")) {
                if(words.contains(s)) msg = msg.replace(s, "*");
            }
        }

        //- The actual chat
        String format = ac.fs().config().getString("settings.chat-format");
        format = format.replace("%prefix%", prefix)
                .replace("%player_nickname%",nameColor + nickname)
                .replace("%title%", title)
                .replace("%suffix%", suffix)
                .replace("%message%", chatColor + msg);

        for(Player pl : Bukkit.getOnlinePlayers()) {
            pl.sendMessage(ChatColor.translateAlternateColorCodes('&', format));
        }
    }
}
