package xyz.aatixx.acorn.modules.region.events.custom;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import xyz.aatixx.acorn.modules.region.Region;

public class RegionLeaveEvent extends Event implements Cancellable {

    private static final HandlerList HANDLERS_LIST = new HandlerList();
    private Player player;
    private Location to, from;
    private Region region;
    private boolean cancelled;

    public RegionLeaveEvent(Player player, Region region, Location to, Location from) {
        this.player = player;
        this.region = region;
        this.to = to;
        this.from = from;
        cancelled = false;
    }

    //- Handler
    @Override
    public HandlerList getHandlers() {
        return HANDLERS_LIST;
    }
    public static HandlerList getHandlerList() {
        return HANDLERS_LIST;
    }

    //- Getters
    public Player getPlayer() { return player; }
    public Region getRegion() { return region; }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        cancelled = b;
         if(!b) player.teleport(from);
    }
}