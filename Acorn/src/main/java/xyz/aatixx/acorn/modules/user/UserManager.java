package xyz.aatixx.acorn.modules.user;

import org.bukkit.entity.Player;
import xyz.aatixx.acorn.Acorn;

import java.util.HashSet;

public class UserManager {

    private Acorn acorn;
    private HashSet<User> users;

    public UserManager(Acorn acorn) {
        this.acorn = acorn;
        this.users = new HashSet<>();
    }

    public HashSet<User> users() { return users; }

    public User create(Player player) {
        User u = new User(this.acorn, player);
        users.add(u);
        return u;
    }

    public User get(Player player) { //TODO :: this is null... for some fucking ass reason...
        return users.stream().filter(u -> u.player().getUniqueId().toString().matches(player.getUniqueId().toString())).findFirst().orElse(null);
    }

    public void remove(Player player) {
        User u = get(player);
        u.save();
        users.remove(u);
    }
}
