package xyz.aatixx.acorn.modules.user.events.user;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.framework.enums.ASetting;
import xyz.aatixx.acorn.modules.user.User;

public class UserQuit implements Listener {

    private Acorn ac;
    public UserQuit(Acorn ac) { this.ac = ac; }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);
        Player player = e.getPlayer();

        //- Time Tracking
        if(ASetting.USER_STAT_TRACKING.b()) {
            User u = this.ac.userManager().get(player);
            u.setPlaytime("logout", System.currentTimeMillis());
            int pt = u.playtime().getInteger("total") + ((int) (u.playtime().getLong("logout") - u.playtime().getLong("login")) / 1000);
            u.setPlaytime("total", pt);
        }

        //- Permissions
        this.ac.permissions().unsetPermissions(player);
        this.ac.userManager().remove(player);
    }
}
