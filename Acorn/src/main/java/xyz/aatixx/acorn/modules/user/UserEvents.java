package xyz.aatixx.acorn.modules.user;

import org.bukkit.plugin.PluginManager;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.framework.enums.ASetting;
import xyz.aatixx.acorn.modules.user.events.stats.*;
import xyz.aatixx.acorn.modules.user.events.user.*;

public class UserEvents {

    public UserEvents(Acorn acorn) {
        PluginManager pm = acorn.getServer().getPluginManager();

        pm.registerEvents(new UserJoin(acorn), acorn);
        pm.registerEvents(new UserQuit(acorn), acorn);
        if(ASetting.CHAT_FORMAT.b()) pm.registerEvents(new UserChat(acorn), acorn);

        //- Stat tracking
        if(ASetting.USER_STAT_TRACKING.b()) {

            //- Movement
            pm.registerEvents(new UserMove(acorn), acorn);

            //- Block
            pm.registerEvents(new UserBlockBreak(acorn), acorn);
            pm.registerEvents(new UserBlockPlace(acorn), acorn);

            //- Kill
            pm.registerEvents(new UserMobKill(acorn), acorn);
            pm.registerEvents(new UserPlayerKill(acorn), acorn);

        }
    }

}
