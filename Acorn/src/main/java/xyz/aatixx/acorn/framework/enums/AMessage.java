package xyz.aatixx.acorn.framework.enums;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import xyz.aatixx.acorn.Acorn;

public enum AMessage {

    RANK_BROADCAST("join.rank-broadcast"),
    ;

    private String index;
    private YamlConfiguration m;

    AMessage(String index) {
        this.index = index;
        this.m = Acorn.api().fs().message();
    }

    public String m() { return ChatColor.translateAlternateColorCodes('&', m.getString(this.index)); }

}
