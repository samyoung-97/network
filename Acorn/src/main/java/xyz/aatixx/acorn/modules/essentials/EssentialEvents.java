package xyz.aatixx.acorn.modules.essentials;

import org.bukkit.plugin.PluginManager;
import xyz.aatixx.acorn.Acorn;
import xyz.aatixx.acorn.modules.essentials.event.message.PingList;

public class EssentialEvents {

    public EssentialEvents(Acorn ac) {
        PluginManager pm = ac.getServer().getPluginManager();

        //- Message
        pm.registerEvents(new PingList(ac), ac);

    }

}